name := """SlackRoller"""

version := "1.7"

scalaVersion := "2.11.7"

// Set of "good" compile options from https://tpolecat.github.io/2014/04/11/scalac-flags.html
scalacOptions ++= Seq(
  "-deprecation",
  "-encoding", "UTF-8",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-language:implicitConversions",
  "-unchecked",
  "-Xfatal-warnings",
  "-Xlint",
  "-Yno-adapted-args",
  "-Ywarn-dead-code",
  "-Ywarn-numeric-widen",
  "-Ywarn-value-discard",
  "-Xfuture",
  "-Ywarn-unused-import",
  "-Yno-predef"
)

scalacOptions in (Compile, console) ~= (_ filterNot (_ == "-Ywarn-unused-import"))
scalacOptions in (Test, console) := (scalacOptions in (Compile, console)).value

initialCommands in console := """
  import scala.Predef._
  import scalaz._
  import roller._
  |""".stripMargin

// Change this to another test framework if you prefer
libraryDependencies ++= Seq(
  "org.parboiled" %% "parboiled" % "2.1.0",
  "io.argonaut" %% "argonaut" % "6.1",
  "org.scalaz" %% "scalaz-core" % "7.1.4",
  "org.scalaz" %% "scalaz-effect" % "7.1.4",
  "org.scalaz" %% "scalaz-concurrent" % "7.1.4",

  // HTTP Server and underlying impl
  "org.http4s" %% "http4s-dsl"          % "0.12.3",
  "org.http4s" %% "http4s-argonaut"          % "0.12.3",
  "org.http4s" %% "http4s-blaze-server" % "0.12.3",

  // HTTP client and related deps
  "com.ning" % "async-http-client" % "1.9.31",
  "io.netty" % "netty" % "3.10.4.Final",
  "org.slf4j" % "slf4j-simple" % "1.7.12",

  // Test deps
  "org.scalatest" %% "scalatest" % "2.2.4" % "test",
  "org.scalacheck" %% "scalacheck" % "1.12.4" % "test"
)

mainClass := Some("roller.WebApp")

dockerRepository := Some("us.gcr.io/pastiche-prod")

enablePlugins(JavaAppPackaging)

enablePlugins(DockerPlugin)

dockerExposedPorts := Seq(8080)


fork in run := true
