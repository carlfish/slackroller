# Usage: #

## When run as a slash command: ##

Responds to the configured slash command. Parse errors will be quietly reported
to the requester without bothering the rest of the channel

For example, if configured against the /roll command:

    /roll 2d20 drop 1 + 4, 1d8 + 2

## When run as a bot: ##

Listens on channel for any line starting with "roll" (in lower-case)

For example, to roll your attack and damage all at once:

    roll 2d20 drop 1 + 4, 1d8 + 2

If the parser can not understand a roll, it will silently ignore it so as not
to annoy people on the channel with false positives.

## Supported roll syntax: ##

* d10 -> roll ten-sided die.
* 2d20 -> roll two twenty-sided dice, add them together
* 4d6 drop 1 -> roll four six-sided dice, drop the lowest roll, sum the rest
* 4d6 keep 3 -> roll four six-sided dice, sum the highest three rolls
* d20 + 6, d8 + 4 -> multiple separate rolls
* 4d6 drop 1 6 times -> repeat rolls
* 4d6 + 4 (or 4d6 - 4) -> constant modifiers

## Mostly supported (display might be a bit confused): ##

* 4d6 + 2d10 (or 4d6 - 1d10) -> combine multiple rolls
* roll 4d6 drop 1 + 1d8 - 3 -> combine complex multiple rolls
