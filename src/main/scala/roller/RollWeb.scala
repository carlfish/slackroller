package roller;

import scalaz._
import scalaz.concurrent.Task
import org.http4s._
import org.http4s.dsl._
import org.http4s.argonaut._
import org.http4s.server.blaze.BlazeBuilder
import _root_.argonaut._, Argonaut._


object WebApp extends App {
    BlazeBuilder.bindHttp(8080, "0.0.0.0")
      .mountService(RollService.service)
      .run
      .awaitShutdown()
}

object RollService extends RollOps {
  def slackSuccess(sender: String, roll: String, results: List[String]): Json =
    ("response_type" := "in_channel") ->:
    ("text" := "<@" + sender + "> rolled " + roll + ":") ->:
    (("attachments", jArrayElements(results.map( a =>
      ("text" := a) ->: jEmptyObject
    ):_*))) ->:
    jEmptyObject

  def slackFail(msg: String): Json = {
    ("text" := msg) ->:
    jEmptyObject
  }

  def nativeSuccess(roll: String, result: String): Json =
    ("roll" := roll) ->:
    ("result" := result) ->:
    jEmptyObject

  def nativeFail(msg: String): Json = {
    ("error" := msg) ->:
    jEmptyObject
  }

  val service = HttpService {
    case req @ GET -> Root => Ok()
    case req @ POST -> Root / "dice" / "slack" => slackRoll(req)
    case req @ POST -> Root / "dice" => nativeRoll(req)
  }

  def slackRoll(req: Request) = {
    req.decode[UrlForm] { data =>
      val roll = data.getFirstOrElse("text", "").trim
      val sender = data.getFirstOrElse("user_name", "").trim

      if (roll.isEmpty) Ok(slackFail("You need to tell me what to roll. Supported syntax can be found at https://bitbucket.org/carlfish/slackroller/src"))
      else parseAndRoll(roll)
        .map(result => if (result.length > 10) Ok(slackFail("That many rolls would be a little too spammy.")) else
          Ok(slackSuccess(sender, roll, result)))
        .getOrElse(Ok(slackFail("Could not parse roll: " + roll + ". Supported syntax can be found at https://bitbucket.org/carlfish/slackroller/src")))
    }
  }

  def nativeRoll(req: Request) = {
    req.decode[UrlForm] { data =>
      val roll = data.getFirstOrElse("roll", "").trim

      if (roll.isEmpty) BadRequest(nativeFail("Missing form field: roll."))
      else parseAndRoll(roll)
        .map(result => Ok(nativeSuccess(roll, result.mkString("\n"))))
        .getOrElse(BadRequest(nativeFail("Could not parse roll: " + roll)))
    }
  }

  def parseAndRoll(s: String): Option[List[String]] = for {
    parsedRoll <- new RollParser(s).RollSpec.run().toOption
  } yield(rollDice(parsedRoll))


  def rollDice: (List[Roll[Int]] => List[String]) = rs => {
    import scalaz.std.list._
    import scalaz.syntax.traverse._

    // Scala's type system gets confused a lot.
    val allrolls: List[Roller[RollDone[Int]]] = rs.map(_.roll)
    allrolls.sequence.run(new RollState(java.util.concurrent.ThreadLocalRandom.current(), 0)) match {
      case -\/(s) => List(s)
      case \/-((_, is)) => is.map(_.desc)
    }
  }
}
