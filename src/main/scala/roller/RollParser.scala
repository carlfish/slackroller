package roller

import org.parboiled2._
import java.lang.Integer.parseInt
import java.util.Random

import scalaz._

import scala.collection.immutable.List

import Predef._

trait RollOps {
    type RollResult[+A] = String \/ A
    type Roller[A] = StateT[RollResult, RollState, A]

    // Push successful and failing values into the roll context
    def point[A](a: A): Roller[A] = State.state(a).lift[RollResult]
    def fail[A](s: String): Roller[A] = StateT[RollResult, RollState, A]
      { _ => -\/(s) }

    // Convenience: a Roller that does nothing.
    val empty: Roller[Unit] = point(())

    // Access and modify the Roller state
    def get: Roller[RollState] = State.get.lift[RollResult]
    def gets[A](f: RollState => A): Roller[A] = State.gets(f).lift[RollResult]
    def modify(f: RollState => RollState): Roller[Unit] =
      State.modify(f).lift[RollResult]
    def put(s: RollState): Roller[Unit] = State.put(s).lift[RollResult]

    // Fail if we have made 100 rolls so far.
    def checkRollCount: Roller[Unit] = {
      def overLimit: Int => Roller[Unit] = rolls =>
        if (rolls >= 100) fail("I can't roll that many dice. My wrists get sore.")
        else empty

      for {
        rolls <- gets(_.rolls)
        _ <- overLimit(rolls)
      } yield ()
    }
}

case class RollDone[A](desc: String, result: A)

case class RollState(rnd: Random, rolls: Int)

sealed trait Operator {
  def symbol: String
  def apply(left: Int, right: Int): Int
};

case object Addition extends Operator {
  override def symbol = "+"
  override def apply(left: Int, right: Int) = left + right
}

case object Subtraction extends Operator {
  override def symbol = "-"
  override def apply(left: Int, right: Int) = left - right
}

sealed trait DropChoice;

case object DropLowest extends DropChoice
case object KeepHighest extends DropChoice
case object DropHighest extends DropChoice
case object KeepLowest extends DropChoice

sealed trait Roll[A] extends RollOps {
  def roll: Roller[RollDone[A]]
}

case class Constant(value: Int) extends Roll[Int] {
  def roll = point(RollDone(value.toString, value))
}

case class Die(sides: Int) extends Roll[Int] {
  def roll =
    if (sides > 1000)
      fail("Sorry, I don't own any " + sides + "-sided dice.")
    else if (sides == 0)
      fail("Sorry, but the last time I tried to roll a zero-sided die, " +
           "I broke the universe.")
    else if (sides == 1)
      fail("Sorry, I left my Möbius dice at home.")
    else for {
      _ <- checkRollCount
      st <- get
      n <- point(st.rnd.nextInt(sides) + 1)
      _ <- put(st.copy(rolls = st.rolls + 1))
    } yield (RollDone(n.toString, n))
}

case class MultiDie(die: Die, count: Int) extends Roll[Seq[Int]] {
  def roll = {
    def roll_(die: Die, count: Int): Roller[List[Int]] =
      if (count == 0)
        point(List())
      else if (count > 20)
        fail("Sorry, I don't own that many d" + die.sides + "s.")
      else for {
        nextRoll <- die.roll
        remainingRolls <- roll_(die, count - 1)
      } yield (nextRoll.result :: remainingRolls)

    if (count == 0)
      fail("java.lang.ArithmeticException: division by z…hahahahaha! " +
           "Damn. Couldn’t keep a straight face.")
    else
      roll_(die, count).map(res => RollDone(res.mkString(", "), res))
  }
}

case class Add(left: Roll[Int], right: Roll[Int], operator: Operator) extends Roll[Int] {
  def asStr(roll: Roll[Int], res: RollDone[Int]): String = roll match {
    case _: Constant => res.desc
    case _: Die => res.desc
    case _ => s"(${res.desc})"
  }

  def roll = for {
      lr <- left.roll
      rr <- right.roll
      tot = operator(lr.result, rr.result)
  } yield (RollDone(s"${asStr(left, lr)} ${operator.symbol} ${asStr(right, rr)} => ${tot}", tot))
}

case class Drop(dice: Roll[Seq[Int]], count: Int, choice: DropChoice)
    extends Roll[Seq[Int]] {

  def splitMe(rs: Seq[Int]): (Seq[Int], Seq[Int]) = choice match {
    case (DropLowest) => rs.sorted.splitAt(count)
    case (DropHighest) => rs.sorted.reverse.splitAt(count)
    case (KeepLowest) => rs.sorted.splitAt(count).swap
    case (KeepHighest) => rs.sorted.reverse.splitAt(count).swap
  }

  def isKeep(dc: DropChoice) = dc == KeepHighest || dc == KeepLowest
  def isDrop(dc: DropChoice) = dc == DropHighest || dc == DropLowest

  def validateDrop(rs: Seq[Int]): Roller[Unit] =
    if (count == 0 && isDrop(choice))
      fail("If no dice are dropped in a forest, do they make a sound?")
    else if (count == 0 && isKeep(choice))
      fail("If I don't keep any dice, I have no dice.")
    else if (count >= rs.length && isDrop(choice))
      fail("Sorry, I can't drop all my dice.")
    else if (count >= rs.length && isKeep(choice))
      fail("Sorry, I can't keep that many dice.")
    else
      empty

  def asString(kept: Seq[Int], dropped: Seq[Int]) =
    kept.mkString(", ") + " [" + dropped.mkString(", ") + "]"

  def roll =
    for {
      rolls <- dice.roll
      _ <- validateDrop(rolls.result)
      (dropped, kept) = splitMe(rolls.result)
    } yield (RollDone(asString(kept, dropped), kept))
}

case class Sum(rolls: Roll[Seq[Int]]) extends Roll[Int] {
  def roll = for {
    result <- rolls.roll
    summed = result.result.sum
  } yield (RollDone(result.desc + " => " + summed, summed))
}

class RollParser(val input: ParserInput) extends Parser {
  def RollSpec = rule { (oneOrMore(RepeatDieSpec).separatedBy(SP ~ ',' ~ SP) ~> ((s: Seq[List[Roll[Int]]]) => s.toList.flatten)) ~ EOI }

  def RepeatDieSpec: Rule1[List[Roll[Int]]] = rule { DieSpec ~ optional(RepeatSpec) ~>
    ((roll: Roll[Int], times: Option[Int]) => List.fill(times.getOrElse(1))(roll))
  }

  def DieSpec: Rule1[Roll[Int]] = rule { SimpleDieSpec ~ optional(PlusMinusSpec) ~>
    ((roll: Roll[Int], plus: Option[(Roll[Int], Operator)]) => plus match {
      case Some((roll2, sign)) => Add(roll, roll2, sign)
      case None => roll
    })
  }

  def RepeatSpec = rule { SP ~ Number ~ SP ~ "times" ~ SP }

  def SimpleDieSpec = rule { SingleDieSpec | MultiDieSpec | ConstantSpec }

  def ConstantSpec = rule { Number ~> Constant }

  def SingleDieSpec = rule { optional('1') ~ 'd' ~ DieNumber ~> Die }

  def MultiDieSpec = rule {
    (Number ~ 'd' ~ DieNumber ~ optional(DropKeepSpec)) ~>
    ((c: Int, s: Int, d: Option[(Int, DropChoice)]) => d match {
      case Some((count, dropchoice)) => Sum(Drop(MultiDie(Die(s), c), count, dropchoice))
      case None => Sum(MultiDie(Die(s), c))
    })
  }

  def DropKeepSpec = rule { DropSpec | KeepSpec }

  def DropSpec = rule { SP ~ "drop" ~ SP ~ (Number ~> ((i: Int) => (i, DropLowest))) }

  def KeepSpec = rule { SP ~ "keep" ~ SP ~ (Number ~> ((i: Int) => (i, KeepHighest))) }

  def DieNumber = rule { capture(Digits) ~> ((s: String) => {
    if (s == "00") 100
    else parseInt(s)
  })}

  def Number = rule { capture(Digits) ~> ((s: String) => parseInt(s)) }

  def Digits = rule { oneOrMore(CharPredicate.Digit) }

  def SP = rule { zeroOrMore(' ') }

  def PlusMinusSpec = rule { PlusSpec | MinusSpec }

  def PlusSpec = rule { SP ~ '+' ~ SP ~ DieSpec ~> ((r: Roll[Int]) => (r, Addition))}

  def MinusSpec = rule { SP ~ '-' ~ SP ~ DieSpec ~> ((r: Roll[Int]) => (r, Subtraction))}
}
