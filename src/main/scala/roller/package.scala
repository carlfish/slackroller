package object roller {
  object Log {
    import org.slf4j.Logger
    import scalaz.concurrent.Task

    private val logger = org.slf4j.LoggerFactory.getLogger(this.getClass)

    def apply(f: Logger => Unit): Task[Unit] = Task { f(logger) }
  }
}
