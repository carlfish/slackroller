package roller

import scalaz._
import scalaz.syntax.either._
import com.ning.http.client._
import com.ning.http.client.ws.WebSocket
import argonaut._, Argonaut._

case class SReply(id: Int, toChan: String, text: String);

object App extends RtmClient with EnvAuthProvider with RollOps {
  lazy val rnd = new java.util.Random()
  var id = 0

  implicit def ReplyCodec: EncodeJson[SReply] = EncodeJson(
    (sr: SReply) =>
      ("id" := sr.id) ->:
      ("channel" := sr.toChan) ->:
      ("type" := "message") ->:
      ("text" := sr.text) ->:
      jEmptyObject
  )

  val listener: (WSEvent => WS[Unit]) = {
    case WSMessage(m) => handleRtmMessage(m)
    case WSOpened => WS.effect(Log { _.info("Opened") })
    case WSError(t) => WS.effect(Log { _.error(t.toString) })
    case WSClosed => WS.effect(Log { _.info("Closed") })
  }

  def toResponse(msg: MessageFrame): WS[Unit] = {
      val response = for {
          roll <- extractRoll(msg.text)
          parsedRoll <- new RollParser(roll).RollSpec.run().toOption
          result <- rollDice(parsedRoll)
        } yield(sendMessageWS(SReply(
          id, msg.fromChan, "<@" + msg.fromUser + "> rolled " + roll + ": \n" + result).asJson.nospaces))

      response.getOrElse(WS.empty)
  }

  def extractRoll(msg: String): Option[String] =
    if (msg.startsWith("roll")) Some(msg.substring(4).trim)
    else None

  def rollDice: (List[Roll[Int]] => Option[String]) = rs => {
    import scalaz.std.list._
    import scalaz.syntax.traverse._

    // Scala's type system gets confused a lot.
    val allrolls: List[Roller[RollDone[Int]]] = rs.map(_.roll)
    allrolls.sequence.run(new RollState(rnd, 0)) match {
      case -\/(s) => Some(s)
      case \/-((_, is)) => Some(is.map(_.desc).mkString("\n"))
    }
  }

  def handleRtmMessage(msg: String): WS[Unit] = {
    val response = for {
       json <- parseWSMsg(msg)
       frame <- parseFrame(json)
       reply <- reactToFrame(frame).right
     } yield (reply)

    response match {
      case -\/(error) => WS.effect(Log { _.error("Something went wrong: " + error.toString) })
      case \/-(success) => success
    }
  }

  def reactToFrame(frame: RtmFrame): WS[Unit] = frame match {
    case msg: MessageFrame => toResponse(msg)
    case _  => WS.empty
  }

  def mainz(args: Array[String]): Unit = {
    val client = new AsyncHttpClient();
    openRtm(listener)(client).flatMap {
        case \/-(_) => Log { _.info("Client exited cleanly") }
        case -\/(x) => Log { _.error("Client exited with error: " + x) }
    }.run
  }
}
