package roller

import com.ning.http.client._
import com.ning.http.client.ws._


import scala.collection.immutable.Map

import scalaz._
import scalaz.syntax.either._
import scalaz.concurrent.Task
import scalaz.Kleisli.kleisli
import argonaut._, Argonaut._

sealed trait ServerError;
case class TransportError(t: Throwable) extends ServerError
case class BadResponse(reason: String, response: Response) extends ServerError
case class PlaceholderError(reason: String) extends ServerError
case class JsonParseError(reason: String, json: String) extends ServerError
case class ParseError(reason: String, json: Json) extends ServerError
case class ApiError(error: Option[Json]) extends ServerError


sealed trait WSEvent;
case class WSMessage(msg: String) extends WSEvent
case object WSOpened extends WSEvent
case class WSError(t: Throwable) extends WSEvent
case object WSClosed extends WSEvent

trait EnvAuthProvider {
   lazy val authToken: Task[String] = Task { sys.env("SLACK_TOKEN") }
}

trait SlackClient {
  val authToken: Task[String]

  val slackApiRoot = "https://slack.com/api/";

  type Result[T] = \/[ServerError, T]

  // An operation over HTTP
  type Http[T] = Kleisli[Task, AsyncHttpClient, T]

  object Http {
    def point[A](a: A): Http[A] = kleisli { _ => Task.now(a) }
    def effect[A](t: Task[A]): Http[A] = kleisli { _ => t }
  }

  // An operation on a WebSocket
  type WS[T] = Kleisli[Task, WebSocket, T]

  object WS {
    def point[A](a: A): WS[A] = kleisli { _ => Task.now(a) }
    def effect[A](t: Task[A]): WS[A] = kleisli { _ => t }
    def empty(): WS[Unit] = effect { Task.now {} }
  }

  def callApi[T](method: String, args: Map[String, String])(implicit codec: CodecJson[T]): Http[Result[T]]  = {
    val pipeline = (r: Response) =>
      expectResponseCode(200)(r) flatMap
      expectJson flatMap
      expectSuccess flatMap
      parseTo[T]

    callMethod(method, args).map(r => r.flatMap(pipeline))
  }

  def pongWS(msg: Array[Byte]): WS[Unit] = kleisli { ws => Task { ws.sendPong(msg); () }}
  def sendMessageWS(msg: String): WS[Unit] = kleisli { ws => Task { ws.sendMessage(msg); () }}

  def parseWSMsg(s: String): \/[JsonParseError, Json] = Parse.parse(s) leftMap (JsonParseError(_, s))

  def callWebSocket(url: String, handler: (WSEvent => WS[Unit])): Http[Result[Unit]] = kleisli { session => {
    def runHandler(ws: WebSocket, event: WSEvent): Unit = handler(event)(ws).run

    Task.async { complete => {
      session.prepareGet(url).execute(
        new WebSocketUpgradeHandler.Builder().addWebSocketListener(new DefaultWebSocketListener() {
          def ws = webSocket
          override def onOpen(wss: WebSocket): Unit = { super.onOpen(wss); runHandler(ws, WSOpened) }
          override def onError(t: Throwable): Unit =  runHandler(ws, WSError(t))
          override def onClose(wss: WebSocket): Unit = { runHandler(ws, WSClosed); super.onClose(wss) }
          override def onMessage(msg: String): Unit = runHandler(ws, WSMessage(msg))
          override def onPing(msg: Array[Byte]): Unit = pongWS(msg)(ws).run
        })
        .addWebSocketListener(new DefaultWebSocketListener() {
          override def onError(t: Throwable): Unit =  { complete(TransportError(t).left.right) }
          override def onClose(wss: WebSocket): Unit = { complete(().right.right); super.onClose(wss) }
        })
        .build)

        ()
      }}
    }
  }

  def simpleGet(uri: String, args: Map[String, String]): Http[Result[Response]] = kleisli { session =>
    import scala.collection.JavaConversions
    import java.util.Collections.singletonList
    val argsM = JavaConversions.mapAsJavaMap(args.mapValues(singletonList(_)))

    Task.async { complete => {
      session
        .prepareGet(uri)
        .setQueryParams(argsM)
        .execute(new AsyncCompletionHandler[Unit]() {
          def onCompleted(r: Response): Unit = complete(r.right.right)
          override def onThrowable(t: Throwable): Unit = {
            complete(\/-(-\/(TransportError(t))));
            super.onThrowable(t);
          }
        })

        ()
      }
    }
  }


  def callMethod(method: String, args: Map[String, String]): Http[Result[Response]] =
    for {
      token <- Http.effect(authToken)
      fullArgs = args + (("token", token))
      result <- simpleGet(slackApiRoot + method, fullArgs)
    } yield (result)


  def expectResponseCode(responseCode: Int): (Response => Result[Response]) =
    response => {
      if (response.getStatusCode == 200)
        response.right
      else
        BadResponse(s"Expected response code ${responseCode}, " +
                        s"was ${response.getStatusCode}",
                        response).left
    }

  def expectJson: (Response => Result[Json]) = response => {
    if (!response.getContentType.startsWith("application/json"))
      BadResponse(s"Expected application/json, " +
                      s"was ${response.getContentType}",
                      response).left
    else
      Parse.parse(response.getResponseBody).leftMap(BadResponse(_, response))
  }

  def expectSuccess: (Json => Result[Json]) = json => {
    val lens = jObjectPL >=> jsonObjectPL("ok") >=> jBoolPL
    val errorLens = jObjectPL >=> jsonObjectPL("error")

    lens.get(json) match {
      case Some(true) => json.right
      case Some(false) => ApiError(errorLens.get(json)).left
      case None => ParseError("No 'ok' field found in API response", json).left
    }
  }

  def parseTo[T](implicit codec: CodecJson[T]): (Json => Result[T]) = json => {
    codec.decodeJson(json).result match  {
      case -\/((error, _)) => ParseError(error, json).left
      case t: \/-[T] => t
    }
  }
}

sealed trait RtmFrame;

case class UnknownFrame(json: Json) extends RtmFrame
case class MessageFrame(fromUser: String, fromChan: String, text: String) extends RtmFrame
case class AckFrame(id: Int, ts: String, text: String) extends RtmFrame
case class AckErrorFrame(id: Int, ts: String, error: ErrorMsg) extends RtmFrame

case class ErrorMsg(code: Int, msg: String)

trait RtmClient extends SlackClient {
  implicit def MessageFrameDec: DecodeJson[MessageFrame] =
    jdecode3L(MessageFrame.apply)("user", "channel", "text")

  implicit def ErrorMsgDec: DecodeJson[ErrorMsg] =
    jdecode2L(ErrorMsg.apply)("code", "msg")

  implicit def AckFrameDec: DecodeJson[AckFrame] =
    jdecode3L(AckFrame)("reply_to", "ts", "text")

  implicit def AckErrorFrameDec: DecodeJson[AckErrorFrame] =
    jdecode3L(AckErrorFrame)("reply_to", "ts", "error")

  case class RtmStartResponse(wsurl: String)
  implicit def RtmStartCodec: CodecJson[RtmStartResponse] =
    casecodec1(RtmStartResponse.apply, RtmStartResponse.unapply)("url")

  def openRtm(listener: (WSEvent => WS[Unit])): Http[Result[Unit]] =
    startRtm.flatMap {
      case \/-(x) => createWebSocket(listener)(x)
      case x: -\/[ServerError] => Http.point(x)
    }

  def createWebSocket(listener: (WSEvent => WS[Unit])): (RtmStartResponse => Http[Result[Unit]]) =
    resp => callWebSocket(resp.wsurl, listener)

  def startRtm: Http[Result[RtmStartResponse]] =
    callApi("rtm.start", Map(("simple_latest", "true"), ("no_unreads", "true")))

  def parseFrame: (Json => \/[ParseError, RtmFrame]) = json => {
    def ghettoDecode[A](json: Json)(implicit dc: DecodeJson[A]): \/[ParseError, A] =
      dc.decodeJson(json).result.leftMap(_._1).leftMap(ParseError(_, json))

    def parseTypedFrame(ftype: String, json: Json): \/[ParseError, RtmFrame] =
      if (ftype == "message")
        ghettoDecode[MessageFrame](json)
      else
        UnknownFrame(json).right

    def parseUntypedFrame(json: Json): \/[ParseError, RtmFrame] = ackMessageType(json) match {
      case Some(true) => ghettoDecode[AckFrame](json)
      case Some(false) => ghettoDecode[AckErrorFrame](json)
      case None => UnknownFrame(json).right
    }

    rtMessageType(json) match {
      case None => parseUntypedFrame(json)
      case Some(s) => parseTypedFrame(s, json)
    }
  }

  def ackMessageType: (Json => Option[Boolean]) = json => {
    val lens = jObjectPL >=> jsonObjectPL("ok") >=> jBoolPL
    lens.get(json)
  }

  def rtMessageType: (Json => Option[String]) = json => {
    val lens = jObjectPL >=> jsonObjectPL("type") >=> jStringPL
    lens.get(json)
  }
}

object slackdebug extends RtmClient with EnvAuthProvider {
  val debugWSListener: (WSEvent => (WebSocket => Unit)) = {
    case WSMessage(m) => _ => Predef.println(m)
    case WSOpened => _ => Predef.println("Opened")
    case WSError(t) => _ => Predef.println(t);
    case WSClosed => _ => Predef.println("Closed")
  }
}
