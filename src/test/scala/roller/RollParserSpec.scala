package roller

import scala.Predef.$conforms
import scala.Stream
import org.scalatest._
import org.scalatest.prop.Checkers
import org.scalacheck.Prop._
import org.scalacheck.Shrink._
import org.scalacheck.{Prop, Gen, Shrink}

import scala.util.Success

class RollParserSpec extends FlatSpec with Checkers with Matchers {
  def posInt = Gen.choose(0, Integer.MAX_VALUE)
  def oneOrMore = Gen.choose(1, Integer.MAX_VALUE)
  def twoOrMore = Gen.choose(2, Integer.MAX_VALUE)
  def maybeSpace = Gen.oneOf("", " ")
  def plusOrMinus = Gen.oneOf(("+", Addition), ("-", Subtraction))

  case class StandardRoll(s: Int, c: Int) {
    def str = s"${c}d${s}"
    def parsesTo = Sum(MultiDie(Die(s), c))
  }

  val standardRoll = for {
    c <- oneOrMore
    s <- twoOrMore
  } yield (StandardRoll(s, c))

  "The roll parser" should "roll constants like a boss" in {
    check { Prop.forAll(posInt)((n: Int) =>
      new RollParser(n.toString).RollSpec.run() == Success(List(Constant(n))))}
  }

  it should "parse bare die rolls" in {
    check { Prop.forAll(oneOrMore)((n: Int) =>
      (new RollParser("d" + n).RollSpec.run() == Success(List(Die(n)))))}
  }

  it should "interpret d00 as a d100" in {
    new RollParser("d00").RollSpec.run() should equal (Success(List(Die(100))))
  }

  it should "interpret xd00 as xd100" in {
    new RollParser("2d00").RollSpec.run() should equal (Success(List(Sum(MultiDie(Die(100), 2)))))
  }

  it should "treat 1dX and dX the same" in {
    check { Prop.forAll(oneOrMore)((n: Int) =>
      (new RollParser("1d" + n).RollSpec.run() == Success(List(Die(n)))))}
  }

  it should "parse multiple die rolls" in {
    check { Prop.forAll(standardRoll)( roll => {
        new RollParser(roll.str).RollSpec.run() ==
          Success(List(roll.parsesTo))}
    )}
  }

  it should "parse dropped rolls" in {
    check { Prop.forAll(oneOrMore, twoOrMore, oneOrMore)(
      (sides: Int, count: Int, drop: Int) =>
        (new RollParser(count + "d" + sides + " drop " + drop).RollSpec.run() ==
          Success(List(Sum(Drop(MultiDie(Die(sides), count), drop, DropLowest)))
        ))
    )}
  }

  it should "parse kept rolls" in {
    check { Prop.forAll(oneOrMore, twoOrMore, oneOrMore)(
      (sides: Int, count: Int, keep: Int) =>
        (new RollParser(count + "d" + sides + " keep " + keep).RollSpec.run() ==
          Success(List(Sum(Drop(MultiDie(Die(sides), count), keep, KeepHighest)))
        ))
    )}
  }

  it should "add or subtract a constant" in {
    check { Prop.forAll(standardRoll, oneOrMore, plusOrMinus, maybeSpace, maybeSpace)(
      (roll: StandardRoll, add: Int, sign: (String, Operator), ws1: String, ws2: String) =>
        new RollParser(roll.str + ws1 + sign._1 + ws2 + add).RollSpec.run() ==
          Success(List(Add(roll.parsesTo, Constant(add), sign._2)))
    ) }
  }

  it should "add or subtract a second roll" in {
    check { Prop.forAll(standardRoll, standardRoll, plusOrMinus, maybeSpace, maybeSpace)(
      (left: StandardRoll, right: StandardRoll, sign: (String, Operator), ws1, ws2) =>
        new RollParser(left.str + ws1 + sign._1 + ws2 + right.str).RollSpec.run() ==
          Success(List(Add(left.parsesTo, right.parsesTo, sign._2)))
    )}
  }

  it should "add or subtract a third roll" in {
    check { Prop.forAll(standardRoll, standardRoll, standardRoll, plusOrMinus, plusOrMinus, maybeSpace, maybeSpace)(
      (one: StandardRoll, two: StandardRoll, three: StandardRoll, s1, s2, ws1, ws2) =>
        new RollParser(one.str + ws1 + s1._1 + ws2 + two.str + ws1 + s2._1 + ws2 + three.str).RollSpec.run() ==
          Success(List(Add(one.parsesTo, Add(two.parsesTo, three.parsesTo, s2._2), s1._2)))
    )}
  }

  it should "append a second roll" in {
    check { Prop.forAll(standardRoll, standardRoll, maybeSpace, maybeSpace)(
      (left, right, ws1, ws2) =>
        new RollParser(left.str + ws1 + "," + ws2 + right.str).RollSpec.run() ==
          Success(List(left.parsesTo, right.parsesTo))
    )}
  }

  it should "append a third roll" in {
    check { Prop.forAll(standardRoll, standardRoll, standardRoll, maybeSpace, maybeSpace)(
      (one, two, three, ws1, ws2) =>
        new RollParser(one.str + ws1 + "," + ws2 + two.str + ws1 + "," + ws2 + three.str).RollSpec.run() ==
          Success(List(one.parsesTo, two.parsesTo, three.parsesTo))
    )}
  }

}
